
const actionSendMessageToUser = (contacto) => {
    return {
        type: "SEND_MESSAGE_TO_USER",
        payload: contacto
    }
}

export default actionSendMessageToUser