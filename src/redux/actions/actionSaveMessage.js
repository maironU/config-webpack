import axios from 'axios'

export const Success = (message) => {
    return {
        type: "SUCCESS_MESSAGE",
        message
    }
}

const SaveMessage = (message, idFrom, idTo) => {
    return (dispatch) =>{ 
        axios.post("http://localhost:5000/apiMessage/nuevo/mensaje",{
            idFrom,
            idTo,
            message
        }).then(response => {
            console.log("recibi mensaje que envie: ", response)
            dispatch(Success([response.data]))
        })
    }
}

export default SaveMessage