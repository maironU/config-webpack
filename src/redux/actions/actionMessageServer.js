import axios from 'axios'

export const MessageServer = (message) => {
    return {
        type: "MESSAGE_SERVER",
        payload: message
    }
}

export const MessageBDD = (messagesBdd) => {
    return {
        type: "MESSAGE_BDD",
        payload: messagesBdd
    }
}

export const FetchMessages = (from, to) =>{
    const idSala = from + to;
    return (dispatch) => {
        axios.get(`http://localhost:5000/apiMessage/mensajes/${from}/${to}`)
             .then(response => {
                console.log([response.data])
                dispatch(MessageBDD(response.data))
            })
    }
}
