import axios from 'axios'
import history from '../../history';
import SweetAlert from 'sweetalert2'
import jwt from 'jsonwebtoken'

export const LOADING_LOGIN = "LOADING"
export const SUCCESS_LOGIN = "SUCCESS"
export const ERROR_LOGIN = "ERROR"

export const SuccessLogin = (user) => {
    return {
        type: "SUCCESS_LOGIN",
        payload: user
    }
}

const FetchLogin = (data, ref) => {
    return (dispatch) => {
        axios.post("http://localhost:5000/api/login", data)
             .then(response => {
                if(response.data.length === 0){
                    SweetAlert.fire({
                        title: "Error",
                        icon: "error",
                        text:"Usuario o contraseña incorrectas",
                    })
                    .then(confirm => {
                        if(confirm){
                            const pass = ref.current;
                            pass.value = ""
                            dispatch(SuccessLogin([]))
                        }
                    })
                }else{
                    localStorage.setItem("token", response.data.token)
                    history.push("/")   
                    const user = jwt.decode(response.data.token)
                    dispatch(SuccessLogin([user]))
                }   
                
             })
    }
}

export default FetchLogin

