import axios from 'axios'
import SweetAlert from 'sweetalert2'
import history from '../../history';

export const LOADING = "LOADING"
export const SUCCESS = "SUCCESS"
export const ERROR = "ERROR"

export const Loading = () => {
    return {
        type: "LOADING_REGISTER"
    }
}

export const Success = (messageCreate) => {
    return {
        type: "SUCCESS_REGISTER",
        payload: messageCreate
    }
}

export const Error = (error) => {
    return {
        type: "ERROR_REGISTER",
        payload: error
    }
}

const FetchRegister = (data) => {
    return (dispatch) => {
        dispatch(Loading())
        axios.post("http://localhost:5000/api/register", data)
             .then(response => {
                console.log(response.data)
                if(response.data.errors){
                    SweetAlert.fire({
                        icon :"error",
                        title: "Error",
                        text:"Todos los campos son requeridos",
                    })
                }else{
                    SweetAlert.fire({
                        icon :"success",
                        title: "Success",
                        text:"Cuenta creada correctamente",
                    })
                    .then(confirm => {
                        if(confirm){
                            history.push('/login')
                        }
                    })
                }
                   
                dispatch(Success([response.data]))
             })
             .catch(error => {
                 dispatch(Error(error))
                 console.log(error)
             })
    }
}

export default FetchRegister

