import { createStore, combineReducers, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import reducerRegister from './reducers/reducerRegister'
import reducerLogin from './reducers/reducerLogin'
import reducerIsToken from './reducers/reducerIsToken'
import reducerContact from './reducers/reducerContact'
import reducerSendMessageToUser from './reducers/reducerSendMessageToUser'
import reducerSaveMessage from './reducers/reducerSaveMessage'
import reducerMessageServer from './reducers/reducerMessageServer'

const reducers = combineReducers ({
    reducerRegister,
    reducerLogin,
    reducerIsToken, 
    reducerContact,
    reducerSendMessageToUser,
    reducerSaveMessage,
    reducerMessageServer
})

const store = createStore(
    reducers,
    composeWithDevTools(
        applyMiddleware(thunk)
    )
)

export default store