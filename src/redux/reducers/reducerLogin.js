import { LOADING_LOGIN, SUCCESS_LOGIN, ERROR_LOGIN } from '../actions/actionLogin'

const initialState = {
    isAutenticated: false,
    user: {},
}

const reducerLogin = (state = initialState, action) => {
    switch(action.type){
        case "SUCCESS_LOGIN":
            return {
                ...state,
                isAutenticated: true,
                user: action.payload
            }
        default:
            return state
    }
}

export default reducerLogin