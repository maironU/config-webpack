const initialState = {
    message: []
}

const reducerMessageServer = (state = initialState, action) => {

    console.log("length: ", state.message.length)
    switch(action.type) {
        case "MESSAGE_SERVER": 
            return {
                ...state,
                message: state.message.concat(action.payload)
            }
        case "MESSAGE_BDD": 
            return {
                ...state,
                message: action.payload
            }
        default: 
            return state

    }
}

export default reducerMessageServer