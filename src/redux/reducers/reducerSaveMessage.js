const initialState = {
    message: []
}

const reducerSaveMessage = (state = initialState, action) => {
    switch(action.type){
        case "SUCCESS_MESSAGE":
            return {
                ...state,
                message: action.payload
            }
        default:
            return state
    }
}

export default reducerSaveMessage