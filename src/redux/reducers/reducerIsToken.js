import { LOADING_LOGIN, SUCCESS_LOGIN, ERROR_LOGIN } from '../actions/actionLogin'

const initialState = {
    user: ""
}

const isToken = (state = initialState, action) => {
    switch(action.type){
        case "isToken":
            return {
                ...state,
                user: action.payload
            }
        default:
            return state
    }
}

export default isToken