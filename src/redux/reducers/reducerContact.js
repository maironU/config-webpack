const initialState = {
    loading: true,
    contacts: []
}

const reducerContact = (state = initialState, action) => {
    switch(action.type){
        case "LOADING_CONTACTS":
            return {
                ...state,
                loading: true,
                contacts: []
            }
        case "SUCCESS_CONTACTS":
            return {
                ...state,
                loading: false,
                contacts: action.payload
            }
        default:
            return state
    }
}

export default reducerContact