
const initialState = {
    contact: []
}

const SendMessageToUser = (state = initialState, action) => {
    switch(action.type){
        case "SEND_MESSAGE_TO_USER":
            return {
                ...state,
                contact: action.payload
            }
        default:
            return state
    }
}

export default SendMessageToUser