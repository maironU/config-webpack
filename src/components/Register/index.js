import React, { useState, useEffect } from 'react'
import { ContainerLogin, ContainerForm, Link, Message } from '../Login/styled'
import { useSelector, useDispatch } from 'react-redux'
import FetchRegister from '../../redux/actions/actionRegister'
import { Redirect } from 'react-router-dom';

const Register = () => {

    const messageCreate = useSelector(state => state.reducerRegister)
    const dispatch = useDispatch()


    useEffect(() => {
        $("input").keypress(function(event) {
            console.log("siiii")
            if (event.which == 13) {
                event.preventDefault();
                Registrarse();
            }
        });
    }, [])

    function Registrarse(){

        console.log("entre")

        var name = document.getElementById("name").value;
        var lastName = document.getElementById("lastName").value;
        var number = document.getElementById("number").value;
        var password = document.getElementById("password").value;

        const data = {
            name,
            lastName,
            number,
            password
        }

        dispatch(FetchRegister(data))
    }

    return(
        <ContainerLogin>
            <h2>Registro</h2>

            <ContainerForm>
                <input type="text" name = "name" id = "name" placeholder = "Nombre"/>
                <input type="text" name = "lastName" id = "lastName" placeholder = "Apellidos"/>
                <input type="number" name = "number" id = "number" placeholder = "Teléfono"/>
                <input type="password" name = "password" id = "password" placeholder = "Contraseña"/>
                <input type = "submit" value = "Registrarse" onClick = {() => Registrarse()}/>
            </ContainerForm>
    
            <span>¿Ya tienes una cuenta?</span>
            <Link to = "/login">Iniciar Sesión</Link>
        </ContainerLogin>
    )
}
export default Register
