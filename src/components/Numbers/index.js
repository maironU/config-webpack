import React, {useEffect} from 'react'
import { ContainerNumbers, Number, Logo, ContainerInfo, ContainerNameAndDate, Name, Date,
    ContainerNumberAndMessages,Phone } from './styled'
import FetchContacts from '../../redux/actions/actionContacts'
import { useSelector, useDispatch } from 'react-redux'
import actionSendMessageToUser from '../../redux/actions/actionSendMessageToUser'

const Numbers = () => {

    const dispatch = useDispatch()
    const data = useSelector(state => state.reducerIsToken)
    const contactos = useSelector(state => state.reducerContact)

    useEffect(() => {
        dispatch(FetchContacts(data.user[0]._id))
    }, [])

    const SendMessageToUser = (contacto) => {
        dispatch(actionSendMessageToUser(contacto))
    }


    return(
        <>
        {contactos.contacts.length > 0 &&
            <>
                {contactos.contacts.map(function(arraycontact, key){
                    return(
                        <ContainerNumbers key = {key}>
                            <>
                            {arraycontact.map(function(contact, key){
                                return(
                                    <Number key= {key} onClick = {() => SendMessageToUser(contact)}>
                                        <Logo src = "https://scontent-yyz1-1.cdninstagram.com/v/t51.2885-15/e35/77331947_2893011124063503_5483528057090871184_n.jpg?_nc_ht=scontent-yyz1-1.cdninstagram.com&_nc_cat=103&_nc_ohc=9nBhZv79z4YAX8PXw_k&oh=f9c3def29676166ead2e414022bb18ee&oe=5EC2217A"/>
                                        <ContainerInfo>
                                            <ContainerNameAndDate>
                                                <Name>{contact.idContact.name}</Name>
                                                <Date>12:54 p.m</Date>
                                            </ContainerNameAndDate>
                                            <ContainerNumberAndMessages>
                                                <Phone>+57 {contact.idContact.number}</Phone>
                                                <span></span>
                                            </ContainerNumberAndMessages>
                                        </ContainerInfo>
                                    </Number>
                                )
                            })  
                            }
                            </>
                    </ContainerNumbers>
                    )
                })

                }
            </>
        }

        </>
    )
}

export default Numbers