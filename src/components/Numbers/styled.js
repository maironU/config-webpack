import styled from 'styled-components'

export const ContainerNumbers = styled.div`
    position: fixed;
    width: 30%;
    margin-top: 10px;    
`
export const Number = styled.div`
    width: 100%;
    background: white;
    display: flex;
    padding-left: 15px;
    box-sizing: border-box;
    align-items: center;
    height: 75px;
    border-right: 0.1px solid #ddd;
    cursor: pointer;
`
export const Logo = styled.img`
    width: 45px;
    height: 45px;
    border-radius: 50%;
`
export const ContainerInfo = styled.div`
    width: 80%;
    border-bottom: 1px solid #ddd;
    padding-left: 10px;
    padding-bottom: 10px;
`
export const ContainerNameAndDate = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 5px;
    box-sizing: boder-box;
`
export const Name = styled.span`
    font-size: 20px;
    font-weight: 600;
`
export const Date = styled.span`
    font-size: 11px;
`
export const ContainerNumberAndMessages = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`
export const Phone = styled.span`
    font-size: 12px;
    font-weight: 600;
`