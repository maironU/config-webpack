import React, { useEffect, useState } from 'react'
import HeaderChat from '../HeaderChat'
import { Container } from './styled'
import InputMessage from '../InputMessage'
import ContentMessages from '../ContentMessages'

const Chat = () => {

    return(
        <Container>
            <HeaderChat />
            <ContentMessages />
            <InputMessage />
        </Container>
    )
}

export default Chat