import React from 'react'
import { Container } from './styled'
import HeaderProfile from '../HeaderProfile'
import SearchContact from '../SearchContact'
import Numbers from '../Numbers'

const Contacts = () => {
    return(
        <Container> 
            <HeaderProfile  />
            <SearchContact />
            <Numbers />
        </Container>
    )
}   

export default Contacts