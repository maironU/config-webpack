import styled from 'styled-components'

export const ContainerSearchContact = styled.div`
    background : #FAFAFA;
    display: flex;
    justify-content: center;
    padding: 7px;
    border-bottom: 0.1px solid #ddd;

`
export const Input = styled.input`
    width: 80%;
    border: none;
    padding: 10px;
    border-radius: 30px;
`
