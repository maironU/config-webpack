import styled from 'styled-components'

export const ContainerHeaderProfile = styled.header`
    background : #F9F9F9;
    display: flex;
    align-items: center;
    padding: 10px;
    border-right: 0.1px solid #ddd;
    border-bottom: 0.1px solid #ddd;
    box-sizing: border-box;
    position: fixed;
    width: 30%;
`
export const LogoProfile = styled.img`
    width: 40px;
    height: 40px;
    border-radius: 50%;
    margin-right: 15px;
`
