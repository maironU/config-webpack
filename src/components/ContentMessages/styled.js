import styled from 'styled-components'

export const ContainerContentMessages = styled.div`
    width: 100%;
    height: calc(100% - 125px);
    background-image: url("https://user-images.githubusercontent.com/15075759/28719144-86dc0f70-73b1-11e7-911d-60d70fcded21.png");
    display: flex;
    flex-direction: column;
    position: relative;
    margin-top: 60px;
    overflow-y: scroll;
`

export const ContainerMessage = styled.div`
    width: fit-content;
    min-width: 15px;
    border-radius: 10px;
    background: ${props => props.color};
    display: flex;
    flex-flow: column wrap;
    padding: 0 10px 0 10px;
    position: relative;
    min-height: 45px;
    margin: 0 30px 15px ${props => props.margin_left};
`

export const Message = styled.p`
    width: fit-content;
    margin: 0;
    word-break: break-all;
`

export const Hour = styled.span`
    width: fit-content;
    position: absolute;
    right: 10px;
    bottom: 0;
    font-size: 10px;
    font-weight: 400;
`