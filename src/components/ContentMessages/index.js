import React, { useEffect, useState } from 'react'
import { ContainerContentMessages, ContainerMessage, Message, Hour } from './styled'
import { useSelector, useDispatch } from 'react-redux'
import {FetchMessages} from '../../redux/actions/actionMessageServer'

const ContentMessages = () => {

    const messages = useSelector(state => state.reducerMessageServer)
    const from = useSelector(state => state.reducerIsToken)
    const to = useSelector(state => state.reducerSendMessageToUser)    

    const dispatch = useDispatch()

    useEffect(() => {
        if(typeof to.contact.idContact != 'undefined'){
            dispatch(FetchMessages(from.user[0]._id, to.contact.idContact._id))
        }
    },[to])

    useEffect(() => {
        var conten = document.getElementById("content")
        conten.scrollTop = conten.offsetHeight + 10000000000
    },[messages.message])

    return (   
        <ContainerContentMessages id="content">
            {messages.message.length > 0 &&
                <>
                    {messages.message.map(function(message, key) {
                        return(
                            <ContainerMessage key = {key} color={from.user[0]._id === message.idFrom._id? "#C9FFBF":"#F8FEF7"}
                            margin_left={from.user[0]._id === message.idFrom._id? "auto":"50px"}>
                                <Message>{message.message}</Message>
                                <Hour></Hour>
                            </ContainerMessage>
                        )
                    })

                    }
                </>
            }
        </ContainerContentMessages>
    )
}

export default ContentMessages