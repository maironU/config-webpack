import React from 'react'
import { ContainerLogo, LogoWpp } from './styled'

const Background = () => {
    return(
        <ContainerLogo>
            <LogoWpp src = "https://es.logodownload.org/wp-content/uploads/2018/10/whatsapp-logo-11.png"/>
            <span>whatsapp web</span>
        </ContainerLogo>
    )
}

export default Background