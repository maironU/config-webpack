import styled from 'styled-components'

export const ContainerLogo = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    margin-top: 40px;

    span {
        font-size: 20px;
        margin-left: 10px;
    }
`
export const LogoWpp = styled.img`
    width: 40px;
    height: 40px;
`
