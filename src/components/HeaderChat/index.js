import React from 'react'
import { ContainerHeaderChat, Logo } from './styled'
import { useSelector } from 'react-redux'

const HeaderChat = () => {

    const data = useSelector(state => state.reducerSendMessageToUser)

    return(
        <>
            {typeof data.contact.idContact !== 'undefined' &&
                <ContainerHeaderChat>
                    <Logo src ="https://fotografias.lasexta.com/clipping/cmsimages02/2019/11/14/66C024AF-E20B-49A5-8BC3-A21DD22B96E6/58.jpg" />
                    <span> {data.contact.idContact.name}</span>
                </ContainerHeaderChat>
            }
        </>
    )
}   

export default HeaderChat