import React, {useRef, useEffect, useState} from 'react'
import { ContainerLogin, ContainerForm, Link } from './styled'
import { useSelector, useDispatch } from 'react-redux'
import FetchLogin from '../../redux/actions/actionLogin'
import {Redirect} from 'react-router-dom'

const Login = () => {

    const dispatch = useDispatch()
    const ref = useRef()


    useEffect(() => {
        $("input").keypress(function(event) {
            if (event.which == 13) {
                event.preventDefault();
                Login();
            }
        });

    }, [])

    function Login (ref) {
        var number = document.getElementById("number").value
        var password = document.getElementById("password").value

        var data = {
            number,
            password
        }

        dispatch(FetchLogin(data,  ref))
    }
    
    return(
            <ContainerLogin>
                <h2>Iniciar sesión</h2>

                <ContainerForm>
                    <input type="number" name = "number" id = "number" placeholder = "Teléfono"/>
                    <input type="password" name = "password" id = "password" placeholder = "Contraseña" ref = {ref}/>
                    <input type = "submit" value = "Iniciar Sesión" onClick = { () => Login(ref)}/>
                </ContainerForm>

                <span>¿No tienes una cuenta?</span><br></br>
                
                <Link to = "/register">Regístrate</Link>
            </ContainerLogin>
    )
}
export default Login
