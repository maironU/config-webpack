import React from 'react'
import { ContainerPreload, Preload, ContainerModal, Modal } from './styled'
import { useSelector } from 'react-redux'

const Preloader = () => {   

    const isLoadingRegister= useSelector((state) => state.reducerRegister)

    return(
        <>      
        {(isLoadingRegister.loading) &&
            <ContainerPreload>
                <Preload>
                </Preload>
            </ContainerPreload>
        }
        </>
    )
} 

export default Preloader