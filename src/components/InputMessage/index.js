import React, { useState, useEffect} from 'react'
import { ContainerInputMessage, Input, Button } from './styled'
import { useDispatch, useSelector } from 'react-redux'
import SaveMessages from '../../redux/actions/actionSaveMessage'
import socketIoClient from 'socket.io-client'
import {MessageServer} from '../../redux/actions/actionMessageServer'

const InputMessage = (props) => {

    const dispatch = useDispatch()
    const user = useSelector(state => state.reducerIsToken)
    const MessageTo = useSelector(state => state.reducerSendMessageToUser)
    const [endpoint, setEndiPoint] = useState("http://192.168.1.3:5000");
    const [messages, setMessages] = useState([]);

    useEffect(() => {
        
        var socket = socketIoClient(endpoint);
        socket.emit("id", user.user[0]._id);

        socket.on("messages", message => { 
            const audioRecibo = document.getElementById("audioRecibo")
            audioRecibo.play()
            dispatch(MessageServer(message))
            
        })
        /*socket.broadcast.emit('message, {
            body,
            from: socket.id 
        })*/
    }, [])

    const SendMessage = () => {
        const mensaje = document.getElementById("input")

        const myMessage = mensaje.value
        const from = user.user[0]._id

        if(mensaje){

            var socket = socketIoClient(endpoint);

            var data = {
                idFrom: user.user[0],
                to: MessageTo.contact.idContact._id,
                message: mensaje.value,
                date: new Date()
            }

            socket.emit("message", data);
            var audioEnvio = document.getElementById("audioEnvio")
            audioEnvio.play()

            mensaje.value = ""
            dispatch(SaveMessages(myMessage, from, MessageTo.contact.idContact._id))
            dispatch(MessageServer(data))
        }
    }

    return(
        <ContainerInputMessage>
            <audio id = "audioEnvio" src = "http://192.168.1.3:4000/src/sound/envio.mp3"></audio>
            <audio id = "audioRecibo" src = "http://192.168.1.3:4000/src/sound/recibo.mp3"></audio>
            <Input type = "text" placeholder = "Escribe un mensaje aquí" id="input"/>
            <Button onClick={() => SendMessage()}>Enviar</Button>
        </ContainerInputMessage>
    )
}

export default InputMessage