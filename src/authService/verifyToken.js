import axios from 'axios'
import history from '../history';

export const Success = (user) => {
    return {
        type: "isToken",
        payload: user
    }
}

const VerifyToken = () => {

    return (dispatch) => {   
        const token = localStorage.getItem("token")
        axios.get("http://localhost:5000/api/secure", {
            headers: {'Authorization': `Bearer ${token}`}
        })
        .then(response => {
            dispatch(Success([response.data.user]))
        })
        .catch(error => {
            if(error) history.push("/login");
        })
    }
}

export default VerifyToken