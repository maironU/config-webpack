import React from 'react'
import { Router, Switch, Route, Redirect } from 'react-router-dom'
import PageLogin from './pages/pageLogin'
import PageRegister from './pages/pageRegister'
import PageDashBoard from './pages/PageDashBoard'
import { Provider } from 'react-redux'
import history from './history';
import store from './redux/store'
import jwt from 'jsonwebtoken'
import axios from 'axios'

const App = () =>{

    const isAuthenticated = () => {
        const token = localStorage.getItem("token");
        const decode = jwt.decode(token)
        let isValid = true

        try {
            isValid = token
        }catch(e) {
            return false
        }
        return isValid
    }

    const RoutePrivate = (props)=> (
        isAuthenticated() ?
        <Route {...props} />
        :<Redirect to="/login" />
    )

    return(
        <Provider store = {store}>
            <Router history = {history}>
                <Switch>
                    <Route exac path = "/login" component = {PageLogin} />
                    <Route exac path = "/register" component = {PageRegister} />
                    <RoutePrivate exact path = "/" component = {PageDashBoard} />
                </Switch>
            </Router>
        </Provider>
    )
}

export default App