import React, {useEffect} from 'react'
import Background from '../components/Background'
import Login from '../components/Login'

const PageLogin = () => {    
    return(
        <>
            <Background />
            <Login />
        </>
    )
}

export default PageLogin