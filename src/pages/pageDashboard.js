import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector, useStore } from 'react-redux'
import verifyToken from '../authService/verifyToken'
import Contacts from '../components/Contacts'
import Chat from '../components/Chat'
import { usersResults } from '../redux/selectors'

const PageDashboard = () => {

    const dispatch = useDispatch()
    const data = useSelector(state => state.reducerIsToken)

    useEffect(() => {
        dispatch(verifyToken())
    }, [])

    return(
        <>{data.user[0] &&
            <div style = {{display: "flex"}}>
                <Contacts />
                <Chat />
            </div>
        }
        </>
    )
}

export default PageDashboard